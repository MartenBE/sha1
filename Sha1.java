package sha1;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import static sha1.Sha1.bytesToHex;

public class Sha1 extends JFrame implements ActionListener, DocumentListener {
    
    JPanel basepanel;
    JTextField txtSha1;
    JPasswordField ptxtPassword;
    JButton btnCopy;
    
    Sha1() {
        
        super("Sha1");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        basepanel = new JPanel();
        basepanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        this.add(basepanel);
        
        ptxtPassword = new JPasswordField();
        ptxtPassword.getDocument().addDocumentListener(this);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.insets = new Insets(10, 10, 10, 10);
        basepanel.add(ptxtPassword, gbc);
        
        txtSha1 = new JTextField();
        txtSha1.setMaximumSize(txtSha1.getPreferredSize());
        txtSha1.setEditable(false);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 10, 0);
        basepanel.add(txtSha1, gbc);
        
        btnCopy = new JButton("Copy");
        btnCopy.addActionListener(this);
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0;
        gbc.insets = new Insets(0, 10, 10, 10);
        basepanel.add(btnCopy, gbc);
        
        this.pack();
        this.setSize(800, this.getPreferredSize().height);
        this.setResizable(false);
    }
    
    public static void main(String[] args) {
        
        new Sha1().setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e) {
        
        StringSelection stringSelection = new StringSelection(txtSha1.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }
    
    @Override
    public void insertUpdate(DocumentEvent e) {
        updateTextfields();
    }
    
    @Override
    public void removeUpdate(DocumentEvent e) {
        updateTextfields();
    }
    
    @Override
    public void changedUpdate(DocumentEvent e) {
        updateTextfields();
    }
    
    private void updateTextfields() {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(new String(ptxtPassword.getPassword()).getBytes());
            byte messageDigest[] = md.digest();
            
            txtSha1.setText(bytesToHex(messageDigest).toLowerCase());
            
        } catch (NoSuchAlgorithmException eNsae) {
            eNsae.printStackTrace();
        }
    }
    
    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
}
